import {Injectable} from '@angular/core';
import {HttpService} from "./http.service";
import {ActivatedRoute, Router} from "@angular/router";


@Injectable()
export class PicturesService {
    private ApiUrls = {
        flicker: 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=e3038f030db17fb5a26b2bf867946734&tags=',
        pixabay: 'https://pixabay.com/api/?key=6929411-80a1ea229e0b643174632331d&q='
    };

    private latestSearches = [];

    constructor(private httpService: HttpService,
                private router: Router,
                private route: ActivatedRoute) {
    }


    public getPicturesFromFlicker(searchTerm) {
        return this.httpService.getPhotosFromAPIs(this.ApiUrls.flicker + searchTerm + '&per_page=500&format=json&nojsoncallback=1').map((res) => {
            let flickerParsed = res.json();
            return {
                'totalPhotos': parseInt(flickerParsed.photos.total),
                'photos': flickerParsed.photos.photo.map((obj) => {
                    return {
                        'imgId': parseInt(obj.id),
                        'url': 'https://farm' + obj.farm + '.staticflickr.com/' + obj.server + '/' + obj.id + '_' + obj.secret + '.jpg'
                    }
                })
            }

        })
    }

    public getPicturesFromPixabay(searchTerm) {
        return this.httpService.getPhotosFromAPIs(this.ApiUrls.pixabay + searchTerm).map((res) => {
            let pixabayParsed = res.json();
            return {
                'totalPhotos': parseInt(pixabayParsed.total),
                'photos': pixabayParsed.hits.map((obj) => {
                    return {
                        'imgId': parseInt(obj.id),
                        'url': obj.webformatURL
                    }
                })
            }

        });
    }

    public getLatestSearches() {
        return this.latestSearches;
    }

    public addCurrentSearchToLatestSearches(currentSearch){
        this.latestSearches.push(currentSearch);
    }
}
