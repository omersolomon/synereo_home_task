import {RouterModule, Routes} from "@angular/router";
import {SingleSearchTermComponent} from "./single-search-term/single-search-term.component";
import {SearchListComponent} from "./search-list/search-list.component";

const appRoutes: Routes = [
    {path: '', redirectTo: 'search-list', pathMatch: 'full'},
    {path: 'search-list', component: SearchListComponent},
    {path: 'single-search', component: SingleSearchTermComponent}
];

export const Routing = RouterModule.forRoot(appRoutes);