import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MaterializeModule} from "angular2-materialize";
import {AppComponent} from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpService} from "../services/http.service";
import {PicturesService} from "../services/pictures.service";
import {HttpModule} from '@angular/http';
import {SingleSearchTermComponent} from './single-search-term/single-search-term.component';
import {SearchListComponent} from './search-list/search-list.component';
import {SearchTermsTableComponent } from './search-list/search-terms-table/search-terms-table.component';
import {MomentModule} from "angular2-moment";
import {Routing} from "./app.routes";

@NgModule({
    declarations: [
        AppComponent,
        SingleSearchTermComponent,
        SearchListComponent,
        SearchTermsTableComponent
    ],
    imports: [
        BrowserModule,
        MaterializeModule,
        ReactiveFormsModule,
        HttpModule,
        Routing,
        MomentModule,
    ],
    providers: [HttpService, PicturesService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
