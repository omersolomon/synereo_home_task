import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PicturesService} from "../../services/pictures.service";
import * as moment from 'moment/moment';
import 'rxjs/add/operator/map';


@Component({
    selector: 'app-single-search-term',
    templateUrl: './single-search-term.component.html',
    styleUrls: ['./single-search-term.component.scss']
})
export class SingleSearchTermComponent implements OnInit {

    public picturesToDisplay = <any>[];
    public pixabaySection: boolean = true;
    public flickerSection: boolean = true;
    public searchTerm;
    public currentSearch = {
        'totalSearches': 0
    };

    constructor(private router: Router,
                private route: ActivatedRoute,
                private picturesService: PicturesService) {
    }

    ngOnInit() {
        this.getParams();
        this.createARequestForApis();
        this.addCurrentSearchToLatestSearches();
    }

    private getParams() {
        this.route.queryParams.subscribe((params: Params) => {
            this.searchTerm = params.termsToSearch;
        });
    }

    private createARequestForApis() {
        //building an object that would contain all details regarding this query
        this.currentSearch['searchTerm'] = this.searchTerm;
        this.currentSearch['searchTime'] = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        this.getPicturesFromPixabay(this.searchTerm);
        this.getPicturesFromFlicker(this.searchTerm);

    }

    private getPicturesFromFlicker(searchTerm) {
        this.picturesService.getPicturesFromFlicker(this.searchTerm).subscribe(
            (res) => {
                this.currentSearch['flickerPics'] = res;
                this.currentSearch['totalSearches'] += res.totalPhotos;
            }
        )
    }

    private getPicturesFromPixabay(searchTerm) {
        this.picturesService.getPicturesFromPixabay(searchTerm).subscribe(
            (res) => {
                this.currentSearch['pixabayPics'] = res;
                this.currentSearch['totalSearches'] += res.totalPhotos;
            })
    }

    private addCurrentSearchToLatestSearches(){
        this.picturesService.addCurrentSearchToLatestSearches(this.currentSearch);
    }
}
