import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSearchTermComponent } from './single-search-term.component';

describe('SingleSearchTermComponent', () => {
  let component: SingleSearchTermComponent;
  let fixture: ComponentFixture<SingleSearchTermComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSearchTermComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSearchTermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
