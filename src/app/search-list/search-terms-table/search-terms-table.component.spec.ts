import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTermsTableComponent } from './search-terms-table.component';

describe('SearchTermsTableComponent', () => {
  let component: SearchTermsTableComponent;
  let fixture: ComponentFixture<SearchTermsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTermsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTermsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
