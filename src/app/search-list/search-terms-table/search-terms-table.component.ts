import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-search-terms-table',
    templateUrl: './search-terms-table.component.html',
    styleUrls: ['./search-terms-table.component.scss']
})
export class SearchTermsTableComponent implements OnInit {

    @Input() latestSearches: any;
    @Output() onRowPick: EventEmitter<any> = new EventEmitter<any>();
    public searchType = {
        'searchTerm': 'asc',
        'searchTime': 'asc',
        'totalSearches': 'asc'
    };

    constructor() {
    }

    ngOnInit() {
    }

    public rowPick(singleSearch): void {
        this.onRowPick.emit(singleSearch.searchTerm);
    }

    sortAsc(sortParameter) {
        this.latestSearches.sort((a, b) => {
            if (a[sortParameter] === b[sortParameter]) {
                return 0;
            }
            else {
                return (a[sortParameter] < b[sortParameter]) ? -1 : 1;
            }
        });
        this.searchType[sortParameter] = 'asc';
    }


    sortDesc(sortParameter) {
        this.latestSearches.sort((a, b) => {
            if (a[sortParameter] === b[sortParameter]) {
                return 0;
            }
            else {
                return (a[sortParameter] > b[sortParameter]) ? -1 : 1;
            }
        });
        this.searchType[sortParameter] = 'desc';

    }



}
