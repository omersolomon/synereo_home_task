import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpService} from "../../services/http.service";
import {PicturesService} from "../../services/pictures.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
    selector: 'app-search-list',
    templateUrl: './search-list.component.html',
    styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {

    searchTermsForm: FormGroup;
    public latestSearches = [];
    public formSent: boolean = false;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private fb: FormBuilder,
                private httpService: HttpService,
                private picturesService: PicturesService) {
    }

    ngOnInit() {
        this.createSearchTermForm();
        this.getLatestSearches();
    }

    private createSearchTermForm() {
        this.searchTermsForm = this.fb.group({
            searchTerm: ['', [Validators.required]]
        })
    }

    public getPhotosFromAPIs(termsToSearch) {
        if (this.searchTermsForm.valid || this.searchTermsForm.controls.searchTerm.pristine) {
            this.formSent = true;
            this.router.navigate(['single-search/'], { queryParams: { termsToSearch: termsToSearch } })
        }
    }

    public getLatestSearches() {
        this.latestSearches = this.picturesService.getLatestSearches();
    }

}
